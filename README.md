# Food Standards Web App for Kotlin #

This version of the Food standards app re-written in the Kotlin Language, with unit tests written in Groovy within the 
Spock framework, as its much more readable

(original JAVA version located here: https://bitbucket.org/janojanahan/food-standards)

### Building the app###

`gradle build clean`

`gradle bootRun`

