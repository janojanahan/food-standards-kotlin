package org.janahan.foodstandards.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonValue
import org.janahan.foodstandards.domain.RatingRegion.ENGLAND_WALES
import org.janahan.foodstandards.domain.RatingRegion.SCOTLAND

data class Authority(
        val idCode: Long,
        val name: String,
        @get:JsonIgnore val region: String?) {

    fun inScotland() = (region ?: "") == "Scotland"
}

data class RatingsSummary(val ratingsList: List<RatingsItem>) {
    val totalEstablishments
        @JsonProperty get() = ratingsList
                .map { it.count }.sum()

    data class RatingsItem(val ratingLevel: HygeneRatingLevel, val count: Int)
}

enum class RatingRegion { SCOTLAND, ENGLAND_WALES }

enum class HygeneRatingLevel constructor(private val displayValue: String,
                                         val textValue: String,
                                         val ratingRegion: RatingRegion) {
    FIVE("5 Stars", "5", ENGLAND_WALES),
    FOUR("4 Stars", "4", ENGLAND_WALES),
    THREE("3 Stars", "3", ENGLAND_WALES),
    TWO("2 Stars", "2", ENGLAND_WALES),
    ONE("1 Stars", "1", ENGLAND_WALES),
    EXEMPT("Exempt", "Exempt", ENGLAND_WALES),
    PASS("Pass", "Pass", SCOTLAND),
    NEEDS_IMPROVEMENT("Needs Improvement", "Improvement Required", SCOTLAND);

    @JsonValue
    fun displayValue(): String {
        return displayValue
    }

    companion object {
        fun fromString(textValue: String) =
                values().find { it.textValue.equals(textValue, true) } ?: EXEMPT

        fun valuesFromRegion(ratingRegion: RatingRegion) = values()
                .asSequence()
                .filter { it.ratingRegion == ratingRegion }
                .toSet()
    }
}