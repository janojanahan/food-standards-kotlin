package org.janahan.foodstandards.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class FsaAuthorities(val authorities: List<FsaAuthority>) {

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class FsaAuthority(
            @get:JsonProperty("LocalAuthorityId") val localAuthorityId: Long,
            @get:JsonProperty("Name") val name: String,
            @get:JsonProperty("RegionName") val regionName: String?
    )
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class FsaEstablishments(val establishments: List<FsaEstablishment>) {

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class FsaEstablishment(
            @get:JsonProperty("RatingValue") val ratingValue: String
    )
}