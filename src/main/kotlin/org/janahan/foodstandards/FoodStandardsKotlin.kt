package org.janahan.foodstandards

import io.netty.channel.ChannelOption
import io.netty.handler.timeout.ReadTimeoutHandler
import io.netty.handler.timeout.WriteTimeoutHandler
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import reactor.netty.tcp.TcpClient
import java.util.concurrent.TimeUnit


@SpringBootApplication
class FoodStandardsKotlin {
    private val REMOTE_SERVER_TIMEOUT_MILLIS: Int = 10000

    @Bean
    fun getTcpClient() = TcpClient.create()
            .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, REMOTE_SERVER_TIMEOUT_MILLIS)
            .doOnConnected { connection ->
                connection.addHandlerLast(ReadTimeoutHandler(REMOTE_SERVER_TIMEOUT_MILLIS.toLong(), TimeUnit.MILLISECONDS))
                        .addHandlerLast(WriteTimeoutHandler(REMOTE_SERVER_TIMEOUT_MILLIS.toLong(), TimeUnit.MILLISECONDS)
                        )
            }
}


fun main(args: Array<String>) {
    runApplication<FoodStandardsKotlin>(*args)
}
