package org.janahan.foodstandards.service

class FsaServerException(message: String, cause: Throwable) : Exception(message, cause)