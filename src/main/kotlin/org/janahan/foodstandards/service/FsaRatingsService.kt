package org.janahan.foodstandards.service

import org.janahan.foodstandards.domain.Authority
import org.janahan.foodstandards.domain.HygeneRatingLevel
import org.janahan.foodstandards.domain.HygeneRatingLevel.Companion.valuesFromRegion
import org.janahan.foodstandards.domain.RatingRegion.ENGLAND_WALES
import org.janahan.foodstandards.domain.RatingRegion.SCOTLAND
import org.janahan.foodstandards.domain.RatingsSummary
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import java.util.*

private fun Authority.ratingSetByRegion() =
            valuesFromRegion(if (inScotland()) SCOTLAND else ENGLAND_WALES)

@Component
class FsaRatingsService(private val repository: FsaRepository) {


    fun ratingsSummaryForAuthority(authorityId: Long): Mono<RatingsSummary> {
        val authorityDetail = repository.findAuthorityById(authorityId)
        val hygeneRatings = repository.findRatingsForAuthority(authorityId)

        return authorityDetail.zipWith(hygeneRatings, ::createSummary)
    }

    private fun createSummary(authority: Authority, hygeneRatings: List<HygeneRatingLevel>): RatingsSummary {
        val regionalRatingSet = authority.ratingSetByRegion()
        val ratingsGrouping = hygeneRatings.asSequence()
                .filter { regionalRatingSet.contains(it) }
                .groupingBy { it }
                .eachCountTo(EnumMap<HygeneRatingLevel, Int>(HygeneRatingLevel::class.java))
        return RatingsSummary(
                regionalRatingSet.map { RatingsSummary.RatingsItem(it, ratingsGrouping[it] ?: 0) })
    }

}