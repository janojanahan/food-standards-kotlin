package org.janahan.foodstandards.service

import org.janahan.foodstandards.domain.Authority
import org.janahan.foodstandards.domain.FsaAuthorities
import org.janahan.foodstandards.domain.FsaAuthorities.FsaAuthority
import org.janahan.foodstandards.domain.FsaEstablishments
import org.janahan.foodstandards.domain.HygeneRatingLevel
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Mono
import reactor.netty.http.client.HttpClient
import reactor.netty.tcp.TcpClient


private fun FsaAuthority.toAuthority() = Authority(localAuthorityId, name, regionName)

private fun FsaAuthorities.toAuthorityList() = authorities.map { it.toAuthority() }

private fun FsaEstablishments.toHygeneRatingList(): List<HygeneRatingLevel> =
        establishments.map { HygeneRatingLevel.fromString(it.ratingValue) }

@Component
class FsaRepository(
        @Value("\${app.external.fsa.baseUrl}") baseUrl: String,
        tcpClient: TcpClient) {

    companion object {
        private val LOG = LoggerFactory.getLogger(FsaRepository::class.java)
        private const val FSA_API_VERSION = "x-api-version"

    }

    private val webClient = buildWebClient(baseUrl, tcpClient)

    private fun <K> handleClientError(throwable: Throwable): Mono<K> {
        val message = "Error connecting to FSA: ${throwable::class.java.canonicalName} ${throwable.message}"
        LOG.error(message)
        return Mono.error<K>(FsaServerException(message, throwable))
    }

    private fun buildWebClient(baseUrl: String, tcpClient: TcpClient) =
            WebClient.builder()
                    .clientConnector(ReactorClientHttpConnector(HttpClient.from(tcpClient)))
                    .baseUrl(baseUrl)
                    .defaultHeader(FSA_API_VERSION, "2")
                    .build()

    fun findAllAuthorities() = webClient.get()
            .uri("/authorities/basic")
            .retrieve()
            .bodyToMono<FsaAuthorities>()
            .onErrorResume { handleClientError(it) }
            .map { it.toAuthorityList() }

    fun findAuthorityById(id: Long) = webClient.get()
            .uri("/authorities/{id}", id)
            .retrieve()
            .bodyToMono<FsaAuthority>()
            .onErrorResume { handleClientError(it) }
            .map { it.toAuthority() }

    fun findRatingsForAuthority(authorityId: Long) = webClient.get()
            .uri {
                it.path("/establishments")
                        .queryParam("localAuthorityId", authorityId)
                        .build()
            }
            .retrieve()
            .bodyToMono<FsaEstablishments>()
            .onErrorResume { handleClientError(it) }
            .map { it.toHygeneRatingList() }

}