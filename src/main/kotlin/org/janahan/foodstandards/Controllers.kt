package org.janahan.foodstandards

import org.janahan.foodstandards.service.FsaRatingsService
import org.janahan.foodstandards.service.FsaRepository
import org.janahan.foodstandards.service.FsaServerException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

abstract class ExeptionHandlingController {
    @ExceptionHandler(Exception::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun errorHandler(e: Exception) = "Unknown Error: ${e.message}"

    @ExceptionHandler(FsaServerException::class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    fun fsaServerExceptionHandler(e: FsaServerException) = e.message ?: ""
}

@RestController
@RequestMapping(path = ["/api/authority"], produces = ["application/json"])
class AuthoritiesController(private val fsaRepository: FsaRepository) : ExeptionHandlingController() {

    @RequestMapping(method = [RequestMethod.GET])
    fun authoritiesList() = fsaRepository.findAllAuthorities()

}

@RestController
@RequestMapping(path = ["/api/authority/{id}/ratings"], produces = ["application/json"])
class RatingsController(private val fsaRatingsService: FsaRatingsService) : ExeptionHandlingController() {

    @RequestMapping(method = [RequestMethod.GET])
    fun getSummary(@PathVariable id: Long) =
            fsaRatingsService.ratingsSummaryForAuthority(id)
}