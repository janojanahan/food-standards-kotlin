foodStandardsApp.service('ApiService', function ($http, $q, $rootScope) {

    var authorities = undefined

    function getAllAuthorities() {
        var deferred = $q.defer()
        if (authorities) {
            deferred.resolve(authorities)
        } else {
            $http.get('/api/authority')
                .then(
                    function (response) {
                        authorities = response.data
                        deferred.resolve(authorities)
                    },
                    function () {
                        deferred.reject()
                    }
                )
        }
        return deferred.promise
    }

    this.filterAuthority = function (query) {
        var deferred = $q.defer()
        getAllAuthorities().then(
            function (authorities) {
                var results = query ? authorities.filter(authoritiesFilter(query)) : authorities
                deferred.resolve(results)
            },
            function () {
                $rootScope.$broadcast(SERVER_ERROR, "Error getting authorities.")
                deferred.reject()
            }
        )
        return deferred.promise
    }


    function authoritiesFilter(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(item) {
            return (item.name.toLowerCase().indexOf(lowercaseQuery) === 0);
        };
    }

    this.getRatings = function (establishmentId) {
        var deferred = $q.defer()
        $http.get('/api/authority/' + establishmentId + '/ratings')
            .then(
                function (response) {
                    ratings = response.data
                    deferred.resolve(ratings)
                },
                function () {
                    deferred.reject()
                }
            )
        return deferred.promise
    }
})
