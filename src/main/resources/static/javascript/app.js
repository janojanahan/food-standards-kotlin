var foodStandardsApp = angular.module('foodStandardsApp', ['ngMaterial'])
    .filter('percentage', ['$filter', function ($filter) {
        return function (input, decimals) {
            return (input > 0 ? $filter('number')(input * 100, decimals) : 0) + '%';
        };
    }])

var SERVER_ERROR = "fsa.serverError"
