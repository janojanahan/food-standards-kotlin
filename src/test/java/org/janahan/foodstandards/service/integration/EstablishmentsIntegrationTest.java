package org.janahan.foodstandards.service.integration;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.jayway.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.janahan.foodstandards.FoodStandardsKotlin;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.jayway.restassured.RestAssured.when;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FoodStandardsKotlin.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        properties = {"app.external.fsa.baseUrl=http://localhost:8089"})
public class EstablishmentsIntegrationTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8089);

    @Value("${local.server.port}")
    int port;

    @Before
    public void setup() {
        RestAssured.port = port;
    }


    @Test
    public void shouldFetchEstablishments() throws IOException {
        givenThat(get(urlEqualTo("/authorities/84"))
                .withHeader("x-api-version", equalTo("2"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("integration_data/english_authority_integration.json")));

        givenThat(get(urlEqualTo("/establishments?localAuthorityId=84"))
                .withHeader("x-api-version", equalTo("2"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("integration_data//establishments_integration.json")));

        when()
                .get("/api/authority/{id}/ratings", 84)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("totalEstablishments", is(11))
                .body("ratingsList", hasSize(6));
    }
}
