package org.janahan.foodstandards.testing.traits

import io.netty.channel.ChannelOption
import io.netty.handler.timeout.ReadTimeoutHandler
import io.netty.handler.timeout.WriteTimeoutHandler
import reactor.netty.tcp.TcpClient

import java.util.concurrent.TimeUnit

trait TcpClientTrait {

    private static final Integer TEST_REMOTE_TIMEOUT = 1000

    TcpClient tcpClient = TcpClient.create()
            .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, TEST_REMOTE_TIMEOUT)
            .doOnConnected { connection ->
        connection.addHandlerLast(new ReadTimeoutHandler(TEST_REMOTE_TIMEOUT, TimeUnit.MILLISECONDS))
                .addHandlerLast(new WriteTimeoutHandler(TEST_REMOTE_TIMEOUT, TimeUnit.MILLISECONDS)
        )
    }

}