package org.janahan.foodstandards.service

import org.janahan.foodstandards.domain.Authority
import org.janahan.foodstandards.domain.RatingsSummary
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import static org.janahan.foodstandards.domain.HygeneRatingLevel.*

class FsaRatingsServiceSpecification extends Specification {

    FsaRepository fsaRepository = Mock(FsaRepository)

    @Subject
    FsaRatingsService service = new FsaRatingsService(fsaRepository)

    def "Should create an English summary"() {
        given:
            long authorityId = 123L
            final List listOfRatings = [ONE, ONE, EXEMPT, TWO, FIVE, TWO, ONE, FIVE, THREE]

        when:
            RatingsSummary summary = service.ratingsSummaryForAuthority(authorityId).block()

        then:
            1 * fsaRepository.findRatingsForAuthority(authorityId) >> Mono.just(listOfRatings)
            1 * fsaRepository.findAuthorityById(authorityId) >>
                    Mono.just(new Authority(authorityId, "test", "London"))
            0 * _

        and:
            with(summary.ratingsList) {
                size() == 6
                it.collect { it.ratingLevel } == [FIVE, FOUR, THREE, TWO, ONE, EXEMPT]
                it.collect { it.count } == [2L, 0L, 1L, 2L, 3L, 1L]
            }
            summary.totalEstablishments == 9L
    }

    def "Should create a Scottish summary"() {
        given:
            long authorityId = 55L
            final List listOfRatings = [PASS, PASS, NEEDS_IMPROVEMENT, PASS, NEEDS_IMPROVEMENT]

        when:
            RatingsSummary summary = service.ratingsSummaryForAuthority(authorityId).block()

        then:
            1 * fsaRepository.findRatingsForAuthority(authorityId) >> Mono.just(listOfRatings)
            1 * fsaRepository.findAuthorityById(authorityId) >>
                    Mono.just(new Authority(authorityId, "test", "Scotland"))
            0 * _

        and:
            with(summary.ratingsList) {
                size() == 2
                it.collect { it.ratingLevel } == [PASS, NEEDS_IMPROVEMENT]
                it.collect { it.count } == [3L, 2L]
            }
            summary.totalEstablishments == 5L
    }
}
