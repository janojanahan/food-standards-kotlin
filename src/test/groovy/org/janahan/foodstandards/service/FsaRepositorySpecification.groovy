package org.janahan.foodstandards.service

import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.janahan.foodstandards.domain.Authority
import org.janahan.foodstandards.domain.HygeneRatingLevel
import org.janahan.foodstandards.testing.traits.TcpClientTrait
import org.junit.Rule
import reactor.core.Exceptions
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static com.github.tomakehurst.wiremock.client.WireMock.*
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import static org.janahan.foodstandards.domain.HygeneRatingLevel.*

class FsaRepositorySpecification extends Specification implements TcpClientTrait {

    @Rule
    WireMockRule wireMockRule = new WireMockRule(options().port(8090))

    @Subject
    FsaRepository repository

    def setup() {

        String baseUrl = "http://localhost:8090"
        repository = new FsaRepository(baseUrl, tcpClient)
    }


    def 'should get a list of Authorities'() {
        given: "An expected response from the FSA Authorities API"
            givenThat(get(urlEqualTo("/authorities/basic"))
                    .withHeader("x-api-version", equalTo("2"))
                    .willReturn(aResponse()
                    .withStatus(200)
                    .withHeader("Content-Type", "application/json")
                    .withBodyFile("response_data/basic_authorities_test.json")))

        when:
            List<Authority> authoritiesList = repository.findAllAuthorities().block()

        then:
            authoritiesList.size() == 14
            authoritiesList.collect { it.name } == ["Aberdeen City", "Aberdeenshire", "Adur", "Allerdale",
                                                    "Amber Valley", "Anglesey", "Angus", "Antrim and Newtownabbey", "Ards and North Down",
                                                    "Argyll and Bute", "Armagh City, Banbridge and Craigavon", "Arun", "Ashfield", "Ashford"]
            authoritiesList.collect { it.idCode } == [197L, 198L, 277L, 158L, 48L, 334L, 199L, 132L, 133L, 200L,
                                                      134L, 278L, 77L, 249L]
        and:
            verify(1, getRequestedFor(urlEqualTo("/authorities/basic"))
                    .withHeader("x-api-version", equalTo("2")))

    }

    @Unroll
    def "Should get Authority: #authorityName by id #id"() {
        given:
            givenThat(get(urlEqualTo("/authorities/$id"))
                    .withHeader("x-api-version", equalTo("2"))
                    .willReturn(aResponse()
                    .withStatus(200)
                    .withHeader("Content-Type", "application/json")
                    .withBodyFile("response_data/$authResponseFile")))

        when:
            Authority authority = repository.findAuthorityById(id).block()

        then:
            authority.name == authorityName
            authority.inScotland() == scottish

        and:
            verify(1, getRequestedFor(urlEqualTo("/authorities/$id"))
                    .withHeader("x-api-version", equalTo("2")))

        where:
            authorityName   | id  | scottish | authResponseFile
            "Westminster"   | 120 | false    | "westminster_authority.json"
            "Aberdeen City" | 197 | true     | "aberdeen_authority.json"
    }

    def "Should report failure if bad data on get authorities call"() {
        given:
            givenThat(get(urlEqualTo("/authorities/basic"))
                    .withHeader("x-api-version", equalTo("2"))
                    .willReturn(aResponse()
                    .withStatus(500)
                    .withHeader("Content-Type", "application/json")
                    .withBody("bad data")))

        when:
            repository.findAllAuthorities().block()

        then:
            def exception = thrown(Exceptions.ReactiveException)
            exception.cause.message == 'Error connecting to FSA: ' +
                    'org.springframework.web.reactive.function.client.WebClientResponseException.InternalServerError ' +
                    '500 Internal Server Error'

        and:
            verify(1, getRequestedFor(urlEqualTo("/authorities/basic"))
                    .withHeader("x-api-version", equalTo("2")))
    }

    def "Should handle connection refused"() {
        given:
            wireMockRule.stop()

        when:
            repository.findAllAuthorities().block()

        then:
            def exception = thrown(Exceptions.ReactiveException)
            exception.cause.message.startsWith("Error connecting to FSA:")
    }

    def 'should handle a timeout due to slow response'() {
        given:

            givenThat(get(urlEqualTo("/authorities/basic"))
                    .withHeader("x-api-version", equalTo("2"))
                    .willReturn(aResponse()
                    .withStatus(200)
                    .withFixedDelay(1200)
                    .withHeader("Content-Type", "application/json")
                    .withBodyFile("response_data/basic_authorities_test.json")))

        when:
            repository.findAllAuthorities().block()

        then:
            def exception = thrown(Exceptions.ReactiveException)
            exception.cause.message.startsWith("Error connecting to FSA: io.netty.handler.timeout.ReadTimeoutException")
        and:
            verify(1, getRequestedFor(urlEqualTo("/authorities/basic"))
                    .withHeader("x-api-version", equalTo("2")))

    }

    def "Should get list of ratings for authority"() throws IOException {

        given:
            givenThat(get(urlEqualTo("/establishments?localAuthorityId=84"))
                    .withHeader("x-api-version", equalTo("2"))
                    .willReturn(aResponse()
                    .withStatus(200)
                    .withHeader("Content-Type", "application/json")
                    .withBodyFile("response_data/establishments_test.json")))
        when:
            List<HygeneRatingLevel> hygeneRatingList = repository.findRatingsForAuthority(84L).block()


        then:
            hygeneRatingList.size() == 11
            hygeneRatingList == [FIVE, FIVE, FOUR, FOUR, TWO, ONE, EXEMPT, THREE, FOUR, FIVE, FIVE]
        and:
            verify(1, getRequestedFor(urlEqualTo("/establishments?localAuthorityId=84"))
                    .withHeader("x-api-version", equalTo("2")))

    }

    def "Should fail correctly on bad response"() throws IOException {

        given:
            givenThat(get(urlEqualTo("/establishments?localAuthorityId=84"))
                    .withHeader("x-api-version", equalTo("2"))
                    .willReturn(aResponse()
                    .withStatus(200)
                    .withHeader("Content-Type", "application/json")
                    .withBody("bad data")))
        when:
            repository.findRatingsForAuthority(84L).block()

        then:
            def exception = thrown(Exceptions.ReactiveException)
            exception.cause.message.startsWith("Error connecting to FSA: org.springframework.core.codec.DecodingException JSON decoding error: ")
        and:
            verify(1, getRequestedFor(urlEqualTo("/establishments?localAuthorityId=84"))
                    .withHeader("x-api-version", equalTo("2")))

    }
}
